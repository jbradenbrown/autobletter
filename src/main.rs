use futures::prelude::*;
use hyper::{Client, Request};
use hyper_tls::HttpsConnector;
use serde_json::Value;
use std::env;
use tokio;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let sid = &args[1];

    let games = get_games().await?;
    let mut user = get_user(&format!("connect.sid={}", sid)).await?;
    let mut bets: Vec<Bet> = games.into_iter().map(|x| Bet::from(x)).collect();
    bets.sort_by(|a, b| b.amount.partial_cmp(&a.amount).unwrap());

    for bet in bets {
        place_bet(bet, &mut user).await?;
    }

    Ok(())
}

#[derive(Debug)]
pub struct Game {
    game_id: String,
    home_team: String,
    away_team: String,
    home_odds: f64,
    away_odds: f64,
}

#[derive(Debug)]
pub struct Bet {
    game_id: String,
    team_id: String,
    amount: f64,
}

#[derive(Debug)]
pub struct User {
    id: String,
    max_bet: f64,
    cookie: String,
    wallet: f64,
}

impl User {
    async fn update(&mut self) -> Result<&mut Self> {
        self.wallet = get_user_rewards(&self.cookie).await?;

        println!("{:?}\n", &self);

        Ok(self)
    }
}

impl From<Game> for Bet {
    fn from(game: Game) -> Self {
        if game.home_odds > game.away_odds {
            Bet {
                game_id: game.game_id,
                team_id: game.home_team,
                amount: kelly_bet(game.home_odds),
            }
        } else {
            Bet {
                game_id: game.game_id,
                team_id: game.away_team,
                amount: kelly_bet(game.away_odds),
            }
        }
    }
}

fn kelly_bet(odds: f64) -> f64 {
    odds.mul_add(2.0, -1.0)
}

async fn get_games() -> Result<Vec<Game>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let body = get_stream().await?;
    let body: Value = serde_json::from_str(&body)?;
    let games = &body["value"]["games"]["tomorrowSchedule"];
    let games: Vec<Game> = games
        .as_array()
        .expect("oops")
        .iter()
        .map(|v| Game {
            game_id: v["id"].as_str().expect("game id not string").to_string(),
            home_team: v["homeTeam"]
                .as_str()
                .expect("home team not string")
                .to_string(),
            away_team: v["awayTeam"]
                .as_str()
                .expect("away team not string")
                .to_string(),
            home_odds: v["homeOdds"].as_f64().expect("home odds not f64"),
            away_odds: v["awayOdds"].as_f64().expect("away odds not f64"),
        })
        .collect();

    println!("{:?}\n", games);

    Ok(games)
}

async fn get_stream() -> Result<String> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let mut res = client
        .get("https://www.blaseball.com/events/streamData".parse()?)
        .await?;

    let body = res.body_mut();

    Ok(body
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .skip_while(|x| future::ready(*x != '{' as u8))
        .take_while(|x| future::ready(*x != '\n' as u8))
        .map(|c| c as char)
        .collect::<String>()
        .await)
}

async fn get_notifications() -> Result<String> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let mut res = client
        .get("https://www.blaseball.com/api/getUserNotifications".parse()?)
        .await?;

    let body = res.body_mut();

    Ok(body
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .skip_while(|x| future::ready(*x != '{' as u8))
        .take_while(|x| future::ready(*x != '\n' as u8))
        .map(|c| c as char)
        .collect::<String>()
        .await)
}

async fn place_bet(bet: Bet, user: &mut User) -> Result<()> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let user = user.update().await?;

    let req = Request::builder()
        .method("POST")
        // .uri("http://127.0.0.1:8080")
        .uri("https://www.blaseball.com/api/bet")
        .header("Cookie", &user.cookie)
        .header("Content-Type", "application/json")
        .body(hyper::Body::from(format!(
            "{{\
                \"gameId\":\"{}\",\
                \"amount\":\"{}\",\
                \"userId\":\"{}\",\
                \"entityId\":\"{}\",\
                \"type\":\"winner\"\
            }}\n",
            bet.game_id,
            (bet.amount * user.wallet).min(user.max_bet) as i32,
            user.id,
            bet.team_id
        )))?;

    println!("{:?}\n", req);
    let mut res = client.request(req).await?;

    let body: String = res
        .body_mut()
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .map(|c| c as char)
        .collect()
        .await;

    println!("{:?}\n", res);
    println!("{:?}\n", body);

    Ok(())
}

async fn mock_place_bet(bet: Bet, max_bet: f64, user: &mut User) -> Result<()> {
    let user = user.update().await?;

    let req =
        Request::post("https://www.blaseball.com/api/bet").body(hyper::Body::from(format!(
            "{{\
                \"gameId\":\"{}\",\
                \"amount\":\"{}\",\
                \"userId\":\"{}\",\
                \"entityId\":\"{}\",\
                \"type\":\"winner\"\
            }}",
            bet.game_id,
            (bet.amount * user.wallet).min(max_bet) as i32,
            user.id,
            bet.team_id
        )))?;

    println!("{:?}\n", req.body());

    Ok(())
}

async fn get_user(cookie: &str) -> Result<User> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let req = Request::builder()
        .uri("https://www.blaseball.com/api/getUser")
        .header("Cookie", cookie)
        .body(hyper::Body::from(""))
        .expect("getUser request failed");

    let mut res = client.request(req).await?;

    let body = res
        .body_mut()
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .skip_while(|x| future::ready(*x != '{' as u8))
        .take_while(|x| future::ready(*x != '\n' as u8))
        .map(|c| c as char)
        .collect::<String>()
        .await;

    let parsed: Value = serde_json::from_str(&body)?;

    Ok(User {
        id: parsed["id"].as_str().unwrap().to_string(),
        max_bet: 20.0 + 20.0 * parsed["maxBetTier"].as_f64().unwrap(),
        cookie: cookie.to_string(),
        wallet: parsed["coins"].as_f64().unwrap(),
    })
}

async fn get_user_rewards(cookie: &str) -> Result<f64> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let req = Request::builder()
        .uri("https://www.blaseball.com/api/getUserRewards")
        .header("Cookie", cookie)
        .body(hyper::Body::from(""))
        .expect("getUser request failed");

    let mut res = client.request(req).await?;

    let body = res
        .body_mut()
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .skip_while(|x| future::ready(*x != '{' as u8))
        .take_while(|x| future::ready(*x != '\n' as u8))
        .map(|c| c as char)
        .collect::<String>()
        .await;

    let parsed: Value = serde_json::from_str(&body)?;

    Ok(parsed["coins"].as_f64().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio_test;
    #[test]
    fn hello_test() {
        println!("hello");
    }
    #[test]
    fn kelly_test() {
        assert_eq!((kelly_bet(0.7) * 10.0).round() / 10.0, 0.4);
    }
    #[test]
    fn get_games_test() {
        println!("{:?}", tokio_test::block_on(get_games()).unwrap());
    }
    #[test]
    fn get_user_test() {}
    #[test]
    fn place_bet_test() {
        // tokio_test::block_on(
        //     place_bet(
        //         Bet {

        //         }
        //     )
        // );
    }
}
